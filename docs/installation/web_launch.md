# Web Launch

## Overview

The web-launch feature uses Custom Protocols to launch the application from a web
browser.

## Registering the Custom Protocol

The first time the custom protocol is encountered, you will be prompted to register it.
This is a one-time process.

If this fails, you can manually register the protocol by following the instructions
below.

## Manually Registering the Custom Protocol

### Linux

1. Close google chrome
2. Reformat the file to be readable by running the following command: `cat
~/.config/google-chrome/<Active Profile Name>/Preferences | python -m json.tool >
~/.config/google-chrome/<Active Profile Name>/Preferences.json`
3. Open the `~/.config/google-chrome/<Active Profile Name>/Preferences` file in a text
editor.
4. Find the `protocol_handler` section of the file.
5. Ensure that the `https://cdn.flywheel.io` entry exists as is below:

    ```json
        "protocol_handler": {
            "allowed_origin_protocol_pairs": {
                "https://cdn.flywheel.io": {
                    "fwlaunch": true
                },
                "https://zoom.us": {
                    "zoommtg": true
                }
            }
        }
    ```

6. Save the file.
7. Relaunch google chrome.
