# Release notes

## 0.1.0

### Major Features

- Native installers for Mac, Windows, and Linux with version number, OS, and arch
- ITK-SNAP is an integrated application on Mac, Windows, and Linux
  - Segmentation use-case-script fully functional to save segmentation results to a new
  Analysis object on Flywheel.
  - Exports an automatically generated CSV file that provides `number_of_voxels` and
  `volume_in_mm3` for each integer value (0,1,2,...)
  - Exports metadata to each segmentation file with those volumetric measures.
- Web-launch feature functional on Mac, Windows, and Linux for direct integration with
the configured web interface of a Flywheel Instance.
  - Enables launching a specific use case for configured apps

### Minor Features

- Gives notification on the Web-Launcher and `fw-app-launcher` instance mismatch.
- On completion, a popup gives the option to take the user to the newly created
Analysis on the Flywheel Instance.
- Displays `fw-app-laucher` version through the "Settings" Button.
- User provenance is part of Analysis output:
`{origin:{'id': 'user_name@institution.yo', 'type': 'user'}` in Analysis.info
- Groups, Projects, and Collections are listed in alphabetic order in their respective
combo boxes.
- The option to make an Analysis input name differ from the output file name through
an Input Dialogue.

### Documentation

- Add `changelog.md`
- Add `release_notes.md`
- Add `installation/web_launch.md`
- Add `installation/linux_installation.md`
- Add `installation/windows_installation.md`
