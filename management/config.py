"""Module to save and load app configuration."""
import json
import logging
import os
from pathlib import Path

log = logging.getLogger(__name__)

FLYWHEEL_CONFIG_DIR = Path.home() / ".config" / "flywheel"
CONFIG_FILE = FLYWHEEL_CONFIG_DIR / "fw_app_launcher.json"

# TODO: validate config file
VERSION_KEY = "Version"
default_config = {
    VERSION_KEY: "0.1.0",  # This gets updated by the pyproject.toml
    "CachePath": "/tmp",
    "CacheDirName": "flywheel_cache",
    "AppsPath": "",
    "QuitWhenDone": True,
    "DeleteCacheWhenDone": True,
}


class Config:
    """Configuration Class."""

    def __init__(self):
        """Class Constructor."""
        self._config = default_config

        version_string = default_config[VERSION_KEY]

        if CONFIG_FILE.exists():
            self.load()
            # Check version here and fix config if necessary
            self._config[VERSION_KEY] = version_string

        else:
            CONFIG_FILE.parent.mkdir(parents=True, exist_ok=True)
            self.save()
            os.chmod(CONFIG_FILE, 0o600)

    def save(self):
        """Save configuration to CONFIG_FILE."""
        with open(CONFIG_FILE, "w") as outfile:
            json.dump(self._config, outfile, indent=4)

    def load(self):
        """Load configuration from CONFIG_FILE."""
        with open(CONFIG_FILE, "r") as infile:
            self._config = json.load(infile)

    def update(self, updates):
        """Update Configuration on disk.

        Args:
            updates (dict): _description_
        """
        for key, val in updates.items():
            if (key not in self._config) or (val != self._config[key]):
                self._config[key] = val
                log.debug("Updating config  {%s: %s}", key, val)
        self.save()

    def get(self, key, default=None):
        """Get specified configuration from key.

        Args:
            key (str): _description_
            default (str, optional): _description_. Defaults to None.

        Returns:
            _type_: _description_
        """
        return self._config.get(key, default)

    def __getitem__(self, item):
        """Get Item.

        Args:
            item (str): _description_

        Returns:
            _type_: _description_
        """
        return self._config[item]

    def __setitem__(self, key, value):
        """Set Item.

        Args:
            key (_type_): _description_
            value (_type_): _description_
        """
        self._config[key] = value
        self.save()

    def __delitem__(self, key):
        """Delete Item in config.

        Args:
            key (_type_): _description_
        """
        del self._config[key]
        self.save()

    def __contains__(self, item):
        """Check for item in Config.

        Args:
            item (_type_): _description_

        Returns:
            _type_: _description_
        """
        return item in self._config

    def __iter__(self):
        """Iterate through config.

        Returns:
            _type_: _description_
        """
        return iter(self._config)
