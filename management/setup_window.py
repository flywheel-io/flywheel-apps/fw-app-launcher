"""Setup Window Module."""
import logging
import os
from pathlib import Path

import flywheel
from PyQt6 import QtWidgets, uic

log = logging.getLogger(__name__)


class SetUp(QtWidgets.QDialog):
    """Initial set up window for the application."""

    def __init__(self, parent=None):
        """Initialize application.

        Args:
            parent (_type_, optional): _description_. Defaults to None.
        """
        super().__init__(parent)

        self.parent = parent
        self.source_dir = Path(os.path.dirname(os.path.realpath(__file__))).parent

        Form, _ = uic.loadUiType(self.source_dir / "resources/setup.ui")
        self.ui = Form()
        self.ui.setupUi(self)
        self.ui.VersionLabelString.setText(self.parent.app.applicationVersion())
        self.ui.InstanceName.setText(self.parent.instance)
        self.ui.AppsPath.setText(self.initialize_apps_path())
        self.ui.InstanceName.editingFinished.connect(self.instance_changed)
        self.ui.CachePath.editingFinished.connect(self.cache_path_changed)
        self.ui.CacheName.editingFinished.connect(self.cache_name_changed)
        self.ui.AppsPath.editingFinished.connect(self.apps_path_changed)
        self.main_window = parent

    def initialize_apps_path(self):
        """Initialize the apps path.

        Returns:
            Path: The path to the apps directory
        """
        user_apps_path = Path(self.parent.config.get("AppsPath"))
        if not user_apps_path.root:
            # Set a default path for the user apps (changable in the settings)
            user_apps_path = Path.home() / "fw-apps"
            self.parent.config["AppsPath"] = str(user_apps_path)

        if not user_apps_path.exists():
            log.warning("Creating user apps directory.")
            user_apps_path.mkdir(exist_ok=True, parents=True)

        return str(user_apps_path)

    def instance_changed(self):
        """_summary_.

        Returns:
            _type_: _description_
        """
        api_key = self.ui.InstanceName.text()
        if self.parent.instance == api_key:
            return  # nothing actually changed so keep existing
        self.parent.instance = api_key.split(":")[0]
        self.parent.ui.plainTextEdit.appendPlainText(
            f"Instance changed to {self.parent.instance}"
        )
        self.parent.ui.InstanceName.setText(self.parent.instance)
        try:
            self.parent.fw_client = flywheel.Client(api_key)
            self.ui.InstanceName.setText(self.parent.instance)
            self.parent.config.update({"api-key": api_key})
        except Exception as e:
            self.parent.ui.plainTextEdit.appendPlainText(str(e))
            log.error(e)
        self.cache_path_changed()
        self.main_window.ui.GroupComboBox.clear()
        self.main_window.groups = self.main_window.fw_client.groups()
        self.main_window.ui.GroupComboBox.addItems(
            [g.label for g in self.main_window.groups]
        )

    def cache_path_changed(self):
        """_summary_."""
        self.parent.config.update({"CachePath": self.ui.CachePath.text()})

        self.parent.cache_dir = (
            Path(self.parent.config["CachePath"])
            / self.parent.config["CacheDirName"]
            / self.parent.instance
        )
        self.parent.ui.plainTextEdit.appendPlainText(
            f"CacheDir changed to {self.parent.cache_dir}"
        )
        self.parent.ui.CachePath.setText(str(self.parent.cache_dir))
        self.parent.tree_management.set_cache_dir(self.parent.cache_dir)

    def cache_name_changed(self):
        """_summary_."""
        self.parent.config.update({"CacheDirName": self.ui.CacheName.text()})
        self.parent.cache_dir = (
            Path(self.parent.config["CachePath"])
            / self.parent.config["CacheDirName"]
            / self.parent.instance
        )
        self.parent.ui.plainTextEdit.appendPlainText(
            f"CacheDir changed to {self.parent.cache_dir}"
        )
        self.parent.ui.CachePath.setText(str(self.parent.cache_dir))

    def apps_path_changed(self):
        """_summary_."""
        self.parent.config.update({"AppsPath": self.ui.AppsPath.text()})
        self.parent.ui.plainTextEdit.appendPlainText(
            f"AppsPath changed to {self.parent.config.get('AppsPath')}"
        )
        self.parent.app_management.fill_app_list()
