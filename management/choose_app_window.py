"""Module for ChooseApp."""
import os
from pathlib import Path

from PyQt6 import QtWidgets, uic
from PyQt6.QtWidgets import QDialogButtonBox


class ChooseApp(QtWidgets.QDialog):
    """Initial set up window for the application."""

    def __init__(self, parent=None):
        """Initialize application."""
        super().__init__(parent)

        self.parent = parent
        self.main_window = parent
        self.source_dir = Path(os.path.dirname(os.path.realpath(__file__))).parent

        Form, _ = uic.loadUiType(self.source_dir / "resources/choose_app.ui")
        self.ui = Form()
        self.ui.setupUi(self)

        self.ui.buttonBox.button(QDialogButtonBox.StandardButton.Ok).setEnabled(False)
