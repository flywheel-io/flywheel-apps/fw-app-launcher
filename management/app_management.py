"""App Management Module."""
import importlib.util
import json
import logging
import platform
import sys
import tempfile
from pathlib import Path

from PyQt6 import QtCore, QtWidgets
from PyQt6.QtWidgets import QDialogButtonBox

log = logging.getLogger(__name__)


def load_module(module_name, file_path):
    """Loads a module from the path."""
    module_path = f"{file_path}/{module_name}.py"
    spec = importlib.util.spec_from_file_location(module_name, module_path)
    module = importlib.util.module_from_spec(spec)
    sys.modules[module_name] = module
    spec.loader.exec_module(module)
    return module


class AppManagement:
    """Class that coordinates all app-related functionality."""

    def __init__(self, main_window, dialog):
        """Initializes and populates all app-related components.

        Args:
            main_window (_type_): _description_
            dialog (AppLauncher): Component-initialized main window.
        """
        self.tmpdir = tempfile.TemporaryDirectory()

        self.main_window = main_window
        self.configs = self.main_window.configs
        self.inputs = self.main_window.inputs
        self.dialog = dialog
        self.ui = dialog.ui
        self.m_ui = main_window.ui
        self.platform = platform.system()
        self.use_script_module = None
        self.app_module = None
        self.use_script = ""
        self.app_paths = dict()
        self.manifest = None
        self.use_scripts = None

        # Initialize ui components
        if self.platform == "Darwin":
            message = "Available on OS X:"
        elif self.platform == "Linux":
            message = "Available on Linux:"
        elif self.platform == "Windows":
            message = "Available on Windows:"
        else:
            message = "System is unknown."

        # TODO: How do I compensate for the "drag select"?
        self.ui.listApps.itemSelectionChanged.connect(self.app_list_change)

        # self.ui.rdNative.setChecked(False)

        self.fill_app_list()

    def fill_app_list(self):
        """Fills app list from directory.

        Look in the apps directory and the users app directory for apps and fill
        the list.
        """
        self.ui.listApps.clear()
        self.ui.buttonBox.button(QDialogButtonBox.StandardButton.Ok).setEnabled(False)

        possible_apps_paths = list(Path(self.main_window.source_dir / "apps").glob("*"))
        user_apps_path = self.main_window.config.get("AppsPath")
        if user_apps_path != "":
            if not Path(user_apps_path).exists:
                log.warning("User Apps Path does not exist: %s", user_apps_path)
                Path(user_apps_path).mkdir(exist_ok=True, parents=True)
            user_apps = list(Path(user_apps_path).expanduser().glob("*"))
            possible_apps_paths += user_apps
        else:
            # Set a default path for the user apps (changable in the settings)
            user_apps_path = Path.home() / "fw-apps"
            user_apps_path.mkdir(exist_ok=True)
            self.main_window.config["AppsPath"] = str(user_apps_path)

        for app_path in possible_apps_paths:
            if app_path.is_dir():
                app_name = app_path.name
                if "-" in app_name:
                    log.error(
                        "App name cannot contain a '-', %s doesn't work for me.",
                        app_name,
                    )
                    continue
                elif app_name == "html":  # skip the documentation folder
                    continue
                else:
                    self.ui.listApps.addItem(app_name)
                    self.app_paths[app_name] = app_path

    def add_inputs_tab(self, manifest_part):
        """Add Inputs Tab.

        Args:
            manifest_part (dict): config["inputs"]
        """
        if self.inputs:
            self.m_ui.tabWidget.removeTab(
                self.m_ui.tabWidget.indexOf(self.main_window.Inputs_tab)
            )
        # Assign multiple references to the same object
        self.inputs = self.main_window.inputs = {}

        self.main_window.Inputs_tab = QtWidgets.QWidget()
        self.main_window.Inputs_tab.setObjectName(f"inputs_tab")

        cur_y = 10
        Y_INC = 30
        for key, val in manifest_part.items():
            self.inputs[key] = dict()

            self.inputs[key]["base"] = val["base"]

            if val["base"] == "file":  # set row of widgets
                self.inputs[key]["label"] = QtWidgets.QLabel(
                    self.main_window.Inputs_tab
                )
                self.inputs[key]["label"].setGeometry(QtCore.QRect(10, cur_y, 120, 16))
                self.inputs[key]["label"].setObjectName(f"inputs_{val['base']}_label")
                self.inputs[key]["label"].setText(key)
                self.inputs[key]["icon"] = QtWidgets.QLabel(self.main_window.Inputs_tab)
                self.inputs[key]["icon"].setGeometry(QtCore.QRect(140, cur_y, 16, 16))
                self.inputs[key]["icon"].setObjectName(f"inputs_{val['base']}_icon")
                self.inputs[key]["icon"].setText("\u24D8")
                self.inputs[key]["icon"].setToolTip(val["description"])
                self.inputs[key]["pushButton"] = QtWidgets.QPushButton(
                    self.main_window.Inputs_tab
                )
                self.inputs[key]["pushButton"].setGeometry(
                    QtCore.QRect(160, cur_y, 260, 30)
                )
                self.inputs[key]["pushButton"].setObjectName(
                    f"inputs_{val['base']}_pushButton"
                )
                self.inputs[key]["pushButton"].clicked.connect(
                    lambda ch, key=key: self.main_window.show_treeview(key)
                )

                cur_y += Y_INC

            else:
                log.debug("ERROR: unknwn base %s", val["base"])

        self.m_ui.tabWidget.addTab(self.main_window.Inputs_tab, "Inputs")

    def add_config_tab(self, manifest_part):
        """Add Config Tab.

        Args:
            manifest_part (dict):  either config["inputs"] or config["config"]

        TODO: Is this repeated?
        """
        if self.main_window.configs:
            self.m_ui.tabWidget.removeTab(
                self.m_ui.tabWidget.indexOf(self.main_window.Configuration_tab)
            )

        # Assign multiple references to the same object
        self.configs = self.main_window.configs = {}

        self.main_window.Configuration_tab = QtWidgets.QWidget()
        self.main_window.Configuration_tab.setObjectName(f"configs_tab")

        cur_y = 10
        Y_INC = 30
        for key, val in manifest_part.items():
            self.configs[key] = dict()
            self.configs[key]["type"] = val["type"]

            if val["type"] == "boolean":
                self.configs[key]["label"] = QtWidgets.QLabel(
                    self.main_window.Configuration_tab
                )
                self.configs[key]["label"].setGeometry(QtCore.QRect(10, cur_y, 120, 16))
                self.configs[key]["label"].setObjectName(f"configs_{val['type']}_label")
                self.configs[key]["label"].setText(key)
                self.configs[key]["icon"] = QtWidgets.QLabel(
                    self.main_window.Configuration_tab
                )
                self.configs[key]["icon"].setGeometry(QtCore.QRect(140, cur_y, 16, 16))
                self.configs[key]["icon"].setObjectName(f"configs_{val['type']}_icon")
                self.configs[key]["icon"].setText("\u24D8")
                self.configs[key]["icon"].setToolTip(val["description"])
                self.configs[key]["checkbox"] = QtWidgets.QCheckBox(
                    self.main_window.Configuration_tab
                )
                self.configs[key]["checkbox"].setGeometry(
                    QtCore.QRect(160, cur_y, 20, 22)
                )
                self.configs[key]["checkbox"].setObjectName(
                    f"configs_{val['type']}_checkBox"
                )
                self.configs[key]["checkbox"].setText("")

            elif val["type"] == "integer":
                self.configs[key]["label"] = QtWidgets.QLabel(
                    self.main_window.Configuration_tab
                )
                self.configs[key]["label"].setGeometry(QtCore.QRect(10, cur_y, 120, 16))
                self.configs[key]["label"].setObjectName(f"configs_{val['type']}_label")
                self.configs[key]["label"].setText(key)
                self.configs[key]["icon"] = QtWidgets.QLabel(
                    self.main_window.Configuration_tab
                )
                self.configs[key]["icon"].setGeometry(QtCore.QRect(140, cur_y, 16, 16))
                self.configs[key]["icon"].setObjectName(f"configs_{val['type']}_icon")
                self.configs[key]["icon"].setText("\u24D8")
                self.configs[key]["icon"].setToolTip(val["description"])
                self.configs[key]["lineEdit"] = QtWidgets.QLineEdit(
                    self.main_window.Configuration_tab
                )
                self.configs[key]["lineEdit"].setGeometry(
                    QtCore.QRect(160, cur_y, 260, 22)
                )
                self.configs[key]["lineEdit"].setObjectName(
                    f"configs_{val['type']}_lineEdit"
                )

            elif val["type"] == "string":
                self.configs[key]["label"] = QtWidgets.QLabel(
                    self.main_window.Configuration_tab
                )
                self.configs[key]["label"].setGeometry(QtCore.QRect(10, cur_y, 120, 16))
                self.configs[key]["label"].setObjectName(f"configs_{val['type']}_label")
                self.configs[key]["label"].setText(key)
                self.configs[key]["icon"] = QtWidgets.QLabel(
                    self.main_window.Configuration_tab
                )
                self.configs[key]["icon"].setGeometry(QtCore.QRect(140, cur_y, 16, 16))
                self.configs[key]["icon"].setObjectName(f"configs_{val['type']}_icon")
                self.configs[key]["icon"].setText("\u24D8")
                self.configs[key]["icon"].setToolTip(val["description"])
                self.configs[key]["lineEdit"] = QtWidgets.QLineEdit(
                    self.main_window.Configuration_tab
                )
                self.configs[key]["lineEdit"].setGeometry(
                    QtCore.QRect(160, cur_y, 260, 22)
                )
                self.configs[key]["lineEdit"].setObjectName(
                    f"configs_{val['type']}_lineEdit"
                )

            else:
                log.debug("Error: unknown type %s", val["type"])

            cur_y += Y_INC

        self.m_ui.tabWidget.addTab(self.main_window.Configuration_tab, "Configuration")

    def add_information_tab(self, manifest):
        """_summary_.

        Args:
            manifest (_type_): _description_
        """
        if self.main_window.Information_tab:
            self.m_ui.tabWidget.removeTab(
                self.m_ui.tabWidget.indexOf(self.main_window.Information_tab)
            )

        self.main_window.Information_tab = QtWidgets.QWidget()
        self.main_window.Information_tab.setObjectName("Information_tab")

        self.infos = dict()
        to_show = {
            "description": "",
            "name": "Name",
            "author": "Author",
            "maintainer": "Maintainer",
            "version": "Version",
            "source": "Source",
            "url": "URL",
            "license": "License",
        }
        cur_y = 10
        all_text = ""
        for key, val in to_show.items():
            self.infos[key] = dict()

            if key == "description":
                self.infos[key]["QTextEdit"] = QtWidgets.QTextEdit(
                    self.main_window.Information_tab
                )
                self.infos[key]["QTextEdit"].setGeometry(
                    QtCore.QRect(10, cur_y, 410, 500)
                )
                self.infos[key]["QTextEdit"].setReadOnly(True)
                self.infos[key]["QTextEdit"].setObjectName("DescriptionTextBrowser")
                all_text = manifest[key] + "\n"

            else:
                all_text += f"\n{val:<15}"
                all_text += f"{manifest[key]}\n"

        self.infos["description"]["QTextEdit"].setPlainText(all_text)
        self.m_ui.tabWidget.addTab(self.main_window.Information_tab, "Information")

    def app_list_change(self):
        """On changing app, update necessaries.

        On selecting a new app from list, load it's manifest, adjust radio buttons to
        available methods.
        For now, those radio buttons are hidden because the window is too small to show
        them. So ha.

        TODO: This seems repeated.
        """
        self.main_window.app.setOverrideCursor(QtCore.Qt.CursorShape.WaitCursor)
        item = self.ui.listApps.currentItem()
        app_name = item.text()
        self.m_ui.AppName.setText(app_name)
        self.m_ui.AlwaysUseThisApp_checkBox.setChecked(False)

        manifest_file = f"{self.app_paths[app_name]}/manifest.json"
        with open(manifest_file, "r") as stream:
            self.manifest = json.load(stream)

        self.main_window.app.restoreOverrideCursor()
        self.ui.buttonBox.button(QDialogButtonBox.StandardButton.Ok).setEnabled(True)

    def choose_the_app(self):
        """Choose the App.

        Is this a repeated function?
        """
        app_name = self.m_ui.AppName.text()
        self.main_window.app.setOverrideCursor(QtCore.Qt.CursorShape.WaitCursor)

        index = f"{self.app_paths[app_name]}/uses/index.html"
        if Path(index).exists():
            self.main_window.help_url = f"file://{index}"
            log.debug("Setting Help URL to %s", self.main_window.help_url)

            self.add_inputs_tab(self.manifest["inputs"])
            self.add_config_tab(self.manifest["config"])
            self.add_information_tab(self.manifest)
            self.m_ui.tabWidget.setTabEnabled(0, False)
            self.m_ui.tabWidget.setTabEnabled(1, False)
            self.m_ui.AnalysisLabelLineEdit.setEnabled(False)

            self.app_module = load_module("run", self.app_paths[app_name])
            self.app_module.start(app_name)

            possible_use_dirs = list(
                Path(self.app_paths[self.m_ui.AppName.text()] / "uses").glob("*")
            )
            # Remove "URL" use script. This is launched via a web browser.
            # TODO: Allow for this to be seen, but disable execution.
            possible_use_dirs = [
                use for use in possible_use_dirs if "URL" not in str(use)
            ]
            self.m_ui.UseScriptComboBox.clear()
            self.use_scripts = [d.name for d in possible_use_dirs if d.is_dir()]
            self.m_ui.UseScriptComboBox.addItems(self.use_scripts)

            if "app-" + self.m_ui.AppName.text() in self.main_window.config:
                config = self.main_window.config["app-" + self.m_ui.AppName.text()]
                if "use script" in config:
                    self.m_ui.UseScriptComboBox.setCurrentText(config["use script"])

            if len(self.use_scripts) == 1:
                self.m_ui.UseScriptComboBox.setCurrentText(self.use_scripts[0])

            if self.m_ui.UseScriptComboBox.currentIndex() == -1:
                self.m_ui.plainTextEdit.appendPlainText("Select a Use Script")

        else:
            log.info("html index does not exist for %s", index)

        self.main_window.app.restoreOverrideCursor()

    def use_script_changed(self):
        """_summary_."""
        if self.m_ui.UseScriptComboBox.count() > 0:
            app_name = self.m_ui.AppName.text()
            use_script = self.use_scripts[self.m_ui.UseScriptComboBox.currentIndex()]
            if use_script != self.use_script:
                self.use_script = use_script
                log.info(f"use script is {self.use_script}")
                self.use_script_module = load_module(
                    "run", f"{self.app_paths[app_name]}/uses/{self.use_script}"
                )

                index = f"{self.app_paths[app_name]}/uses/{self.use_script}/index.html"
                if Path(index).exists():
                    self.main_window.help_url = f"file://{index}"
                    log.debug("Setting Help URL to %s", self.main_window.help_url)
                else:
                    log.info("html index does not exist for %s", index)

                # This used to pass "self" but now it just passes the string app_name
                self.use_script_module.start(app_name)

                self.main_window.config.update(
                    {"app-" + self.m_ui.AppName.text(): {"use script": self.use_script}}
                )
                self.m_ui.plainTextEdit.appendPlainText(f"Use Script is {use_script}")
