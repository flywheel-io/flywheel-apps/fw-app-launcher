"""Analysis Management Module."""
import json
import logging
import os
import shutil
import webbrowser
from datetime import datetime

import bson
from PyQt6.QtWidgets import QMessageBox

log = logging.getLogger(__name__)


class AnalysisManagement:
    """Class that coordinates all local analysis-related functionality."""

    def __init__(self, main_window):
        """Initializes and populates all app-related components.

        Args:
            main_window (AppLauncher): Component-initialized main window.
        """
        self.main_window = main_window
        self.inputs = main_window.inputs
        self.configs = main_window.configs
        self.app_management = main_window.app_management
        self.ui = main_window.ui
        self.analysis_base_dir = None
        self.analysis_dir = None
        self.file_mod_times = None
        self.config_file = None
        self.gear_config = None
        self.ui.RunGearPushButton.clicked.connect(self.run_gear)
        self.ui.AnalysisPushButton.clicked.connect(
            lambda ch, key=None: self.main_window.show_treeview(key, set_analysis=True)
        )

    def set_destination(self, known_dest=None):
        """Initialize a local 'analysis' and set its destination."""
        app_text = self.main_window.ui.AppName.text()
        analysis_label = app_text + ": " + str(datetime.now()) + " fw-app-launched"
        log.info("New Analysis: %s", analysis_label)
        self.main_window.ui.AnalysisLabelLineEdit.setText(analysis_label)

        self.analysis_base_dir = self.main_window.cache_dir / "Analyses"
        self.analysis_base_dir.mkdir(parents=True, exist_ok=True)

        self.analysis_dir = self.analysis_base_dir / str(bson.ObjectId())
        self.config_file = self.analysis_dir / "config.json"
        for dir_name in ["input", "output", "work"]:
            (self.analysis_dir / dir_name).mkdir(parents=True)

        # Create initial configuration file
        if known_dest:
            id_to_use = known_dest
        else:
            id_to_use = self.main_window.tree_management.current_item.data()
        self.gear_config = {
            "inputs": {},
            "destination": {  # this is the parent of the new analysis
                "id": id_to_use,
                # "type": self.main_window.tree_management.current_item.container_type,
            },
            "config": {},
        }

        with open(self.config_file, "w") as fp:
            json.dump(self.gear_config, fp, indent=4)

    def set_input(self, input_key, file_entry):
        """_summary_.

        Args:
            input_key (str): _description_
            file_entry (FileEntry): _description_
        """
        full_input_dir = self.analysis_dir / "input" / input_key
        self.gear_config["inputs"][input_key] = {
            "hierarchy": {
                "id": file_entry.parent.id,
                "type": file_entry.parent.container_type,
            },
            # "object": self.main_window.fw_client.api_client.sanitize_for_serialization(file_entry.to_dict()),
            "object": {
                "type": file_entry.type,
                "mimetype": file_entry.mimetype,
                "modality": file_entry.modality,
                "classification": file_entry.classification,
                "tags": file_entry.tags,
                "info": file_entry.info,
                "size": file_entry.size,
                "zip_member_count": file_entry.zip_member_count,
                "version": file_entry.version,
                "file_id": file_entry.file_id,
                "origin": {
                    "type": file_entry.origin.type,
                    "id": file_entry.origin.id,
                },
            },
            "location": {"path": str(full_input_dir), "name": file_entry.name},
            "base": "file",
        }

        with open(self.config_file, "w") as fp:
            json.dump(self.gear_config, fp, indent=4)

    def set_config(self):
        """_summary_."""
        for key, val in self.main_window.configs.items():
            if val["type"] == "boolean":
                self.gear_config["config"][key] = val["checkbox"].isChecked()
            elif val["type"] == "integer" and val["lineEdit"].text():
                self.gear_config["config"][key] = int(val["lineEdit"].text())
            elif val["type"] == "string" and val["lineEdit"].text():
                self.gear_config["config"][key] = val["lineEdit"].text()

        with open(self.config_file, "w") as fp:
            json.dump(self.gear_config, fp, indent=4)

    def delete_cached_analysis(self):
        """__summary__."""
        log.info(
            "Deleting Cached Analysis: %s",
            self.main_window.ui.AnalysisLabelLineEdit.text(),
        )
        shutil.rmtree(self.analysis_dir, ignore_errors=True)
        self.main_window.ui.AnalysisLabelLineEdit.setText("Analysis Deleted")
        self.main_window.ui.AnalysisLabelLineEdit.setEnabled(False)
        self.ui.RunGearPushButton.setEnabled(False)

    def download_as_input(self, file_item, k):
        """Download file item as input k.

        Args:
            file_item (FileItem): File item in tree to download
            k (str): input to download as
        """
        full_input_dir = self.analysis_dir / "input" / k
        msg_display = self.ui.plainTextEdit
        _download_as_input(file_item, full_input_dir, msg_display)

    def completed_analysis_url(self):
        """Provide a complete URL to the new Analysis.

        Returns:
            str: URL to the new Analysis on the instance.
        """
        instance = self.main_window.instance
        destination_id = self.gear_config["destination"]["id"]
        destination_container = self.main_window.fw_client.get(destination_id)
        destination_type = destination_container.container_type

        if destination_type == "project":
            analysis_url = f"https://{instance}/#/projects/{destination_id}/analyses"
        else:
            project_id = destination_container.parents["project"]
            analysis_url = f"https://{instance}/#/projects/{project_id}/"
            if destination_type == "subject":
                analysis_url += f"subjects/{destination_id}?tab=analyses"
            elif destination_type == "session":
                analysis_url += f"sessions/{destination_id}?tab=analyses"
            # container_type == "acquisition"
            else:
                session_id = destination_container.parents["session"]
                analysis_url += f"sessions/{session_id}?tab=analyses"

        return analysis_url

    def run_gear(self):
        """_summary_."""
        log.debug("Running Gear")
        self.inputs = self.main_window.inputs
        self.configs = self.main_window.configs
        # Inputs and destination have been chosen in config.json in show_treeview()
        self.set_config()
        # download the selected inputs
        for k, v in self.inputs.items():
            if "item" in v:
                self.download_as_input(v["item"], k)

        self.ui.plainTextEdit.appendPlainText(
            f"Running {self.ui.AppName.text()} (waiting for it to quit)"
        )

        self.app_management.use_script_module.before(self.main_window)

        # E.g. The main() code in apps/ITK-SNAP/run.py actually runs ITK-SNAP
        stdout, stderr, error_code = self.app_management.app_module.main(
            self.main_window
        )

        self.app_management.use_script_module.after(self.main_window)

        # TODO: Prevent uploading an empty analysis???
        # Upload outputs by uploading the analysis
        self.upload_analysis_and_outputs(stdout, stderr, error_code)

        # On Success, report destination
        completed_url = self.completed_analysis_url()
        message = f"View Completed Analysis on {self.main_window.instance}?"
        msg_box = QMessageBox()
        msg_box.setIcon(QMessageBox.Icon.Information)
        msg_box.setText(message)
        msg_box.setWindowTitle("Link to Analysis in Flywheel")
        msg_box.setStandardButtons(
            QMessageBox.StandardButton.Ok | QMessageBox.StandardButton.Cancel
        )
        return_value = msg_box.exec()

        if return_value == QMessageBox.StandardButton.Ok:
            webbrowser.open(completed_url)

        # Delete the analysis
        if self.ui.DeleteCacheWhenDone_checkBox.isChecked():
            self.delete_cached_analysis()

        if self.ui.QuitWhenDone_checkBox.isChecked():
            self.main_window.quit_fw_app_launcher(0)

    def upload_analysis_and_outputs(self, stdout, stderr, error_code):
        """_summary_.

        Args:
            stdout (_type_): _description_
            stderr (_type_): _description_
            error_code (_type_): _description_
        """
        _upload_analysis_and_outputs(
            self.main_window.fw_client,
            self.analysis_dir,
            self.config_file,
            self.main_window.ui.AnalysisLabelLineEdit.text(),
            stdout,
            stderr,
            error_code,
        )


# This function belongs to Analysis Management
def _download_as_input(file_item, full_input_dir, msg_display):
    """Add file to gear input directory.

    Args:
        file_item (FileItem): File item in tree to use.
        full_input_dir (Path): Path to gear input directory.
        msg_display (QTextBox): Text box for logging.
    """
    file_path = full_input_dir / file_item.file.name
    if not file_path.exists():
        msg = f"Downloading file: {file_item.file.name}"
        log.info(msg)
        msg_display.appendPlainText(msg)
        symlink_path, _ = file_item._add_to_cache()
        if not file_path.parent.exists():
            os.makedirs(file_path.parents[0])
        # Follow_symlinks=True ensures a true copy of the file is made
        # This prevents altering the hierarchy cache between runs by saving an output
        # as an input file name. Analysis cache is deleted by default.
        shutil.copy(symlink_path, file_path.parents[0], follow_symlinks=True)
    else:
        msg = f"File already downloaded: {file_item.file.name}"
        log.info(msg)
        msg_display.appendPlainText(msg)


def _upload_analysis_and_outputs(
    fw_client,
    analysis_dir,
    config_file,
    analysis_name,
    stdout,
    stderr,
    error_code,
):
    """_summary_.

    Args:
        fw_client (flywheel.Client): _description_
        analysis_dir (Pathlike): _description_
        config_file (Pathlike): _description_
        analysis_name (str): _description_
        stdout (_type_): _description_
        stderr (_type_): _description_
        error_code (_type_): _description_
    """
    output_dir = analysis_dir / "output"
    outputs = []
    for path in output_dir.glob("*"):
        if path.is_file() and not path.name.startswith("."):
            outputs.append(str(path))

    if len(outputs) == 0:
        log.info("No outputs so not uploading analysis")
        return

    with open(config_file, "r") as fp:
        config = json.load(fp)

    log.info("Uploading analysis and outputs to Flywheel")
    analysis_parent = fw_client.get(config["destination"]["id"])
    input_files = []
    for k, v in config["inputs"].items():
        # resolve file reference
        container = fw_client.get(v["hierarchy"]["id"])
        filename = v["location"]["name"]
        file_ref = container.get_file(filename).ref()
        # append to input_files object
        input_files.append(file_ref)

    log.info(
        "Uploading analysis %s with %s inputs",
        analysis_name,
        len(input_files),
    )
    # TODO: Prevent upload if no output files???
    info = {"origin": {"type": "user", "id": fw_client.get_current_user().id}}
    analysis = analysis_parent.add_analysis(
        label=analysis_name, inputs=input_files, info=info
    )

    if outputs:
        analysis.upload_file(outputs)
        log.info("Uploaded outputs to analysis")
    else:
        log.warning("no analysis outputs were found")
