"""Tree Management Module."""
import logging
from pathlib import Path

from PyQt6 import QtGui, QtWidgets
from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QAbstractItemView, QDialogButtonBox

from .fw_container_items import (
    AcquisitionItem,
    AnalysisFolderItem,
    CollectionItem,
    ContainerItem,
    ContainerParentModel,
    FileItem,
    GroupItem,
    HierarchyItem,
    ProjectItem,
    SessionItem,
    SubjectItem,
)

log = logging.getLogger(__name__)


class TreeManagement:
    """Class that coordinates all tree-related functionality."""

    def __init__(self, main_window, cache_dir):
        """Initialize TreeView object from Main Window.

        Args:
            main_window (QtWidgets.QMainWindow): [description]
            cache_dir (Path): Path to cache directory.
        """
        log.debug("Init TreeManagement")
        self.main_window = main_window
        self.ui = main_window.tree_dialog.ui
        self.cache_dir = cache_dir
        self.current_item = None
        self.cache_files = {}
        self.set_analysis = False
        self.treeView = self.ui.treeView
        tree = self.treeView
        tree.setEditTriggers(QAbstractItemView.EditTrigger.NoEditTriggers)
        tree.clicked.connect(self.tree_clicked)
        tree.doubleClicked.connect(self.tree_dblclicked)
        tree.expanded.connect(self.on_expanded)

        tree.setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu)
        tree.customContextMenuRequested.connect(self.open_menu)
        self.source_model = ContainerParentModel()
        # The cache directory is cascaded to the individual item objects.
        self.set_cache_dir(cache_dir)
        tree.setModel(self.source_model)
        HierarchyItem.set_icon_dir(Path(__file__).parents[1] / "resources")

    def set_cache_dir(self, cache_dir):
        """Set the cache_dir property and cascade to the tree source model.

        Args:
            cache_dir (Path): The cache_dir to set.
        """
        self.cache_dir = Path(cache_dir)
        self.source_model.set_cache_dir(self.cache_dir)

    def get_cache_dir(self):
        """Get the cache_dir property.

        Returns:
            Path: Get the cache_dir property.
        """
        return self.cache_dir

    def tree_clicked(self, index):
        """Cascade the tree clicked event to relevant tree node items.

        Args:
            index (QtCore.QModelIndex): Index of tree item clicked.
        """
        item = self.get_id(index)
        if isinstance(item, ContainerItem):
            self.current_item = item

    def tree_dblclicked(self, index):
        """Cascade the double clicked signal to the tree node double clicked.

        Args:
            index (QtCore.QModelIndex): Index of tree node double clicked.
        """
        item = self.get_id(index)
        if isinstance(item, AnalysisFolderItem):
            item._dblclicked()

    def populateTree(self):
        """Populate the tree starting with groups."""
        groups = self.main_window.fw_client.groups()
        for group in groups:
            group_item = GroupItem(self.source_model, group)

    def expand_all_visible_items(self):
        """Expand all visible items in the tree."""
        proxy = self.source_model
        for row in range(proxy.rowCount()):
            index = proxy.index(row, 0)
            self.treeView.expand(index)

    def populate_tree_from_collection(self, collection):
        """Populate Tree from a single Collection.

        Args:
            collection (flywheel.Collection): Collection to populate tree with.

        Returns:
            CollectionItem: Returns the CollectionItem object.
        """
        collection_item = CollectionItem(self.source_model, collection)
        self.treeView.setEnabled(True)
        self.expand_all_visible_items()

        return collection_item

    def populate_tree_from_project(self, project):
        """Populate Tree from a single Project.

        Args:
            project (flywheel.Project): Project to populate tree with.

        Returns:
            ProjectItem: Returns the ProjectItem object.
        """
        project_item = ProjectItem(self.source_model, project)
        self.treeView.setEnabled(True)
        self.expand_all_visible_items()

        return project_item

    def get_id(self, index):
        """Retrieve the tree item from the selected index.

        Args:
            index (QtCore.QModelIndex): Index from selected tree node.

        Returns:
            QtGui.QStandardItem: Returns the item with designated index.
        """
        item = self.source_model.itemFromIndex(index)
        id = item.data()
        # I will want to move this to "clicked" or "on select"
        self.ui.txtID.setText(id)
        item_type = type(item)
        if self.set_analysis:
            if item_type in (
                ProjectItem,
                SubjectItem,
                SessionItem,
                AcquisitionItem,
            ):
                self.ui.buttonBox.button(QDialogButtonBox.StandardButton.Ok).setEnabled(
                    True
                )
            else:
                self.ui.buttonBox.button(QDialogButtonBox.StandardButton.Ok).setEnabled(
                    False
                )
        else:
            if item_type == FileItem:
                self.ui.buttonBox.button(QDialogButtonBox.StandardButton.Ok).setEnabled(
                    True
                )
            else:
                self.ui.buttonBox.button(QDialogButtonBox.StandardButton.Ok).setEnabled(
                    False
                )
        return item

    def open_menu(self, position):
        """Function to manage context menus.

        Args:
            position (QtCore.QPoint): Position right-clicked and where menu rendered.
        """
        indexes = self.treeView.selectedIndexes()
        if len(indexes) > 0:
            hasFile = False
            for index in indexes:
                item = self.source_model.itemFromIndex(index)
                if isinstance(item, FileItem):
                    hasFile = True

            menu = QtWidgets.QMenu()
            if hasFile:
                action = menu.addAction("Cache Selected Files")
                action.triggered.connect(self._cache_selected)
            menu.exec(self.treeView.viewport().mapToGlobal(position))

    def _cache_selected(self):
        """Cache selected files to local directory."""
        # TODO: Acknowledge this is for files only or change for all files of selected
        #       Acquisitions.
        indexes = self.treeView.selectedIndexes()
        if len(indexes) > 0:
            for index in indexes:
                item = self.source_model.itemFromIndex(index)
                if isinstance(item, FileItem):
                    _, _ = item._add_to_cache()

    def on_expanded(self, index):
        """Triggered on the expansion of any tree node.

        Used to populate subtree on expanding only.  This significantly speeds up the
        population of the tree.

        Args:
            index (QtCore.QModelIndex): Index of expanded tree node.
        """
        item = self.source_model.itemFromIndex(index)
        if hasattr(item, "_on_expand"):
            item._on_expand()

    def cache_selected_for_open(self):
        """Cache selected files (entire acq??) if necessary for opening in application.

        TODO: I may want to rework this according to what files are needed where:
            * View-Only?
            * As a part of analysis.
            * Do I want to add selected files to an analysis via context menu?
        """
        tree = self.ui.treeView
        self.cache_files.clear()
        for index in tree.selectedIndexes():
            item = self.source_model.itemFromIndex(index)
            if isinstance(item, FileItem):
                file_path, _ = item._add_to_cache()

                # TODO: Handle only NIfTIs for now.
                # if ".zip" in str(file_path):
                #     input_zip = ZipFile(file_path, "r")
                #     zip_folder = Path(str(file_path).replace(".zip", ""))
                #     if not zip_folder.exists():
                #         os.makedirs(zip_folder)
                #     input_zip.extractall(zip_folder)

                self.cache_files[item.container.id] = str(file_path)

    def cache_inputs(self):
        """Cache inputs to gear directory."""
        for k, v in self.main_window.inputs.items():
            if "item" in v:
                file_path, _ = v["item"]._add_to_cache()
                self.cache_files[v["item"].container.id] = str(file_path)
