#!/usr/env python3
"""App Launcher Window Module."""
import json
import logging
import os
import os.path as op
import sys
import webbrowser
from pathlib import Path

import flywheel
from flywheel_gear_toolkit.interfaces.command_line import exec_command
from PyQt6 import QtCore, QtWidgets, uic

from .analysis_management import AnalysisManagement
from .app_management import AppManagement, load_module
from .choose_app_window import ChooseApp
from .config import Config
from .msg_management import MsgManagement
from .setup_window import SetUp
from .tree_management import TreeManagement
from .tree_view_dialog import TreeViewDialog

log = logging.getLogger(__name__)


class FileLaunchEvent(QtCore.QEvent):
    """Helper class for launching files."""

    def __init__(self, url):
        """Initialize the event.

        Args:
            url (QUrl): URL of the file to launch.
        """
        super().__init__(QtCore.QEvent.Type.FileOpen)
        self._url = url

    def url(self):
        """Return the url object.

        Returns:
            QUrl: The url object.
        """
        return self._url


class AppLauncher(QtWidgets.QMainWindow):
    """Main window for the application."""

    def __init__(self, app):
        """Initialize application.

        Args:
            app (QApplication): Fw_application object.
        """
        super(AppLauncher, self).__init__()

        # Initialize object variables
        self.source_dir = Path(op.dirname(os.path.realpath(__file__))).parent
        self.config = Config()
        self.app = app
        self.group = None
        self.groups = list()
        self.project = None
        self.projects = list()
        self.project_labels = None
        self.project_item = None
        self.collection = None
        self.collections = list()
        self.collection_labels = None

        # Attempt to acquire api key and flywheel client
        api_key = ""
        user_json = Path.home() / ".config/flywheel/user.json"
        if user_json.exists():  # user is already logged in
            try:
                with open(user_json) as json_file:
                    data = json.load(json_file)
                    api_key = data["key"]
            except KeyError:
                pass

        if not api_key and "api-key" in self.config:
            api_key = self.config["api-key"]

        if api_key:
            self.fw_client = flywheel.Client(api_key)
            self.config.update({"api-key": api_key})
            self.instance = api_key.split(":")[0]
        else:
            self.instance = ""

        self.version = self.config["Version"]
        self.cache_dir = (
            Path(self.config["CachePath"]) / self.config["CacheDirName"] / self.instance
        )
        log.debug("Cache directory is: %s", self.cache_dir)

        self.inputs = None
        self.Inputs_tab = None
        self.configs = None
        self.Configuration_tab = None
        self.Information_tab = None

        index = f"{str(self.source_dir)}/apps/html/index.html"
        self.help_url = f"file://{index}"

        Form, _ = uic.loadUiType(self.source_dir / "resources/app_launcher.ui")
        self.ui = Form()
        self.ui.setupUi(self)

        self.setup = SetUp(self)

        self.tree_dialog = TreeViewDialog(self)
        self.tree_management = TreeManagement(self, self.cache_dir)
        self.choose_app = ChooseApp(self)
        self.app_management = AppManagement(self, self.choose_app)

        if not self.instance:
            if not self.setup.exec():
                sys.exit(1)

        # if len(sys.argv) > 0:
        #    self.run_command_line(sys.argv)

        self.choose_app = ChooseApp(self)
        self.app_management = AppManagement(self, self.choose_app)
        self.msg_management = MsgManagement(self)
        self.analysis_management = AnalysisManagement(self)

        self.connect_controls()
        self.initialize_controls()
        if not app.file_open_event:
            self.ui.plainTextEdit.appendPlainText(
                f"No open event, url is\n{app.url.toString() if app.url else ''}\n"
                "Loading config..."
            )
            self.load_values_from_config()

        self.ui.plainTextEdit.appendPlainText("Sys Arguments " + str(sys.argv[1:]))
        self.ui.plainTextEdit.appendPlainText("App Sys Arguments " + str(app.args))

    def run_use(self, app_name, keyvals):
        """Run the indicated app with the parameters in keyvals.

        Args:
            app_name (str): Name of the app to run.
            keyvals (dict): Parameters to pass to the app.
        """
        msg = f"Running {app_name} with {keyvals}"
        log.info(msg)
        self.ui.plainTextEdit.appendPlainText(str(msg))

        self.ui.AppName.setText(
            app_name
        )  # Name is used in name of new analysis that is created

        manifest_file = f"{self.app_management.app_paths[app_name]}/manifest.json"
        with open(manifest_file, "r") as stream:
            self.app_management.manifest = json.load(stream)

        self.app_management.app_module = load_module(
            "run", self.app_management.app_paths[app_name]
        )
        self.app_management.app_module.start(app_name)

        self.app_management.use_script_module = load_module(
            "run", f"{self.app_management.app_paths[app_name]}/uses/{keyvals['use']}"
        )

        # Set up inputs and config by calling start() in the use case's run.py
        # start() returns True if the use case is valid, False otherwise
        if self.app_management.use_script_module.start(self, keyvals):
            # run the gear
            self.app_management.use_script_module.before(self)

            # E.g. The main() code in apps/ITK-SNAP/run.py actually runs ITK-SNAP
            stdout, stderr, error_code = self.app_management.app_module.main(self)

            self.app_management.use_script_module.after(self)

            # TODO: Prevent uploading an empty analysis???
            # Upload outputs by uploading the analysis
            self.analysis_management.upload_analysis_and_outputs(
                stdout, stderr, error_code
            )

            # delete local copies
            self.analysis_management.delete_cached_analysis()
        else:
            log.warning("Could not run %s", app_name)
            log.warning("Please check your configuration and try again.")
            # Bring up an Alert box
            dlg = QtWidgets.QMessageBox(self)
            dlg.setWindowTitle("Could not run app!")
            dlg.setText("Please check your configuration and try again.")
            dlg.setIcon(QtWidgets.QMessageBox.Icon.Critical)
            dlg.exec()
        self.quit_fw_app_launcher(0)

    def run_via_url(self, url):
        """Handle running app using URL.

        The url is expected to have enough information for the local app to do what it
        needs to do.

        Examples:
            PyQt6.QtCore.QUrl('fwlaunch://textedit?destination=63926ba0834277e556903fce
            &file_name=a_text_file.txt')
                A text file must be found on the given container

            PyQt6.QtCore.QUrl('fwlaunch://itk_snap?use=URL
            &destination=639266a18cdd05613d904114&file_name=T1_SAG_SE.nii.gz')
                The app is "ITK_SNAP" and the use case is "URL".  The code in
                apps/ITK_SNAP/run.py, apps/ITK_SNAP/fw_gear_itk_snap, and
                /apps/ITK_SNAP/uses/URL/run.py must be able to use the destination
                container and file name to find input file(s), create an analysis as
                output and put resulting segmentation files there.  The Flywheel
                Extension that creates this URL can provide additional arguments as
                necessary.

        Args:
            url (QUrl): The URL that contains the information needed to run the app.
        """
        # Parse url to see what is to be done
        url_str = url.toString()
        STARTSW = "fwlaunch://"
        if not url_str.startswith(STARTSW):
            msg = f"url_str does not start with {STARTSW}\n{url_str}"
            log.info(msg)
            self.ui.plainTextEdit.appendPlainText(str(msg))
            return

        url_s1 = url_str.split("/")

        # Linux "fwlaunch://ITK_SNAP?"
        # Windows "fwlaunch://ITK_SNAP/?"
        if "?" in url_s1[2]:
            app_name = url_s1[2].split("?")[0]
            rest = url_s1[2].split("?")[1]
        else:
            app_name = url_s1[2]
            rest = url_s1[3][1:]
        vals = rest.split("&")
        keyvals = {v[0]: v[1] for v in [val.split("=") for val in vals]}

        all_is_well = True
        if not keyvals.get("destination"):
            msg = "destination is missing"
            log.error(msg)
            self.ui.plainTextEdit.appendPlainText(str(msg))
            all_is_well = False
        if not keyvals.get("file_name"):
            msg = "file_name is missing"
            log.warning(msg)
            self.ui.plainTextEdit.appendPlainText(str(msg))

        if all_is_well:
            # URI passed in has app name in all lower case so get lower case
            # versions of what is available
            app_names = {n.lower(): n for n in self.app_management.app_paths.keys()}
            if app_name in app_names:
                app_name = app_names[
                    app_name
                ]  # replace with actual app name possibly with upper case
                if "use" in keyvals:
                    use_path = (
                        self.app_management.app_paths[app_name]
                        / "uses"
                        / keyvals["use"]
                    )
                    if use_path.exists():
                        self.run_use(app_name, keyvals)
                    else:
                        msg = (
                            f"use name {keyvals['use']} not found in "
                            f"apps/uses/{app_name}"
                        )
                        log.error(msg)
                        self.ui.plainTextEdit.appendPlainText(str(msg))
                else:
                    log.error("'use' is missing from the url.  A use case is required.")
            else:
                msg = f"app name {app_name} not found in apps/"
                log.error(msg)
                self.ui.plainTextEdit.appendPlainText(str(msg))
        else:
            msg = f"all was not well"
            log.error(msg)
            self.ui.plainTextEdit.appendPlainText(str(msg))

    def connect_controls(self):
        """Connect gui controls to functions."""
        QtCore.QMetaObject.connectSlotsByName(self)

        # Choose Instance Button
        self.ui.InstanceName.clicked.connect(self.show_setup)

        # Choose Cache Path Button
        self.ui.CachePath.clicked.connect(self.show_setup)

        # Choose App Button
        self.ui.AppName.clicked.connect(self.show_choose_app)

        # Settings and Help Buttons
        self.ui.helpButton.clicked.connect(self.show_help)
        self.ui.settingsButton.clicked.connect(self.show_setup)

        # Checkbox controls for "Always use this app", "Quit when done",
        # and "Delete cache when done"
        self.ui.AlwaysUseThisApp_checkBox.stateChanged.connect(
            self.AlwaysUseThisApp_changed
        )
        self.ui.QuitWhenDone_checkBox.stateChanged.connect(self.QuitWhenDone_changed)
        self.ui.DeleteCacheWhenDone_checkBox.stateChanged.connect(
            self.DeleteCacheWhenDone_changed
        )

        # Use Script ComboBox
        self.ui.UseScriptComboBox.currentIndexChanged.connect(
            self.app_management.use_script_changed
        )
        # Group and Project ComboBoxes
        self.ui.GroupComboBox.currentIndexChanged.connect(self.group_changed)
        self.ui.ProjectComboBox.currentIndexChanged.connect(self.project_changed)

        # Collection Controls
        self.ui.useCollections.clicked.connect(self.onProjectsOrCollections)
        self.ui.useProjects.clicked.connect(self.onProjectsOrCollections)
        self.ui.CollectionComboBox.currentIndexChanged.connect(self.collection_changed)

    def initialize_controls(self):
        """Initialize controls to values from config file."""
        if self.config.get("QuitWhenDone"):
            self.ui.QuitWhenDone_checkBox.setChecked(True)

        if self.config.get("DeleteCacheWhenDone"):
            self.ui.DeleteCacheWhenDone_checkBox.setChecked(True)

        self.ui.InstanceName.setText(self.instance)

        self.ui.CachePath.setText(str(self.cache_dir))
        self.ui.AnalysisIconLabel.setText("\u24D8")
        self.ui.AnalysisPushButton.setEnabled(False)

        self.ui.tabWidget.setTabEnabled(0, False)
        self.ui.tabWidget.setTabEnabled(1, False)
        self.ui.AnalysisLabelLineEdit.setEnabled(False)

        # NOTE: When would groups be initialized before this?
        if not self.groups:
            self.groups = self.fw_client.groups()
            self.groups.sort(key=lambda x: x.label.lower())
            self.group_labels = [g.label for g in self.groups]
            for group in self.groups:
                self.ui.GroupComboBox.addItem(group.label, group)
        else:
            self.group_labels = [g.label for g in self.groups]

        # Read in collections from the flywheel client and populate the combo box
        if not self.collections:
            self.collections = self.fw_client.collections()
            self.collections.sort(key=lambda x: x.label.lower())
            # TODO: Talk to Andy about this way of initializing ComboBoxes
            for collection in self.collections:
                self.ui.CollectionComboBox.addItem(collection.label, collection)
            # if self.collections:
            #     self.ui.CollectionComboBox.setCurrentIndex(0)
            # TODO: Check if we ever need these anywhere at all
            self.collection_labels = [g.label for g in self.collections]
        else:
            self.collection_labels = [g.label for g in self.collections]

    def load_values_from_config(self):
        """Load values from config file."""
        # Initialize the app used from the config file or the dialog
        if (
            "always_use_app" in self.config
            and self.config["always_use_app"] in self.config
        ):
            the_app_name = self.config["always_use_app"][4:]
            items = self.app_management.ui.listApps.findItems(
                the_app_name, QtCore.Qt.MatchFlag.MatchExactly
            )
            if len(items) != 1:
                log.critical("There can be only one!")
                sys.exit(1)
            self.app_management.ui.listApps.setCurrentItem(items[0])
            self.ui.AppName.setText(the_app_name)
            self.app_management.choose_the_app()
            self.ui.AlwaysUseThisApp_checkBox.setChecked(True)

        # Initialize the group from the config file, if it exists
        if "group" in self.config:
            if self.config["group"] in self.group_labels:
                self.ui.plainTextEdit.appendPlainText(
                    f"Previously used group is {self.config['group']}"
                )
                self.ui.GroupComboBox.setCurrentText(
                    self.config["group"]
                )  # sets self.group to Flywheel Group
        else:
            self.ui.plainTextEdit.appendPlainText("Select a group")

        # Initialize the project from the config file, if it exists
        if (
            "project" in self.config
            and self.project_labels
            and not self.ui.useCollections.isChecked()
        ):
            if self.config["project"] in self.project_labels:
                self.project = self.config["project"]
                self.ui.plainTextEdit.appendPlainText(
                    f"Previously used project is {self.config['project']}"
                )
                self.ui.ProjectComboBox.setCurrentText(
                    self.project
                )  # sets self.project to Flywheel Project
            else:
                if self.group:
                    self.ui.plainTextEdit.appendPlainText("Select a project")

        # Initialize the collection from the config file, if it still exists
        if (
            "collection" in self.config
            and self.collection_labels
            and self.ui.useCollections.isChecked()
        ):
            if self.config["collection"] in self.collection_labels:
                self.collection = self.config["collection"]
                self.ui.plainTextEdit.appendPlainText(
                    f"Previously used collection is {self.config['collection']}"
                )
                self.ui.CollectionComboBox.setCurrentText(self.collection)
        else:
            self.ui.plainTextEdit.appendPlainText("Select a collection")

    def show_help(self):
        """_summary_."""
        webbrowser.open(self.help_url)

    def AlwaysUseThisApp_changed(self):
        """_summary_."""
        if self.ui.AlwaysUseThisApp_checkBox.isChecked():
            if self.app_management.ui.listApps.currentItem().text():
                self.config[
                    "always_use_app"
                ] = f"app-{self.app_management.ui.listApps.currentItem().text()}"
            else:
                self.config["always_use_app"] = "None"
        else:
            self.config["always_use_app"] = "None"
        self.config.update({"always_use_app": self.config["always_use_app"]})

    def QuitWhenDone_changed(self):
        """_summary_."""
        if self.ui.QuitWhenDone_checkBox.isChecked():
            self.config["QuitWhenDone"] = True
        else:
            self.config["QuitWhenDone"] = False
        self.config.update({"QuitWhenDone": self.config["QuitWhenDone"]})

    def DeleteCacheWhenDone_changed(self):
        """_summary_."""
        if self.ui.DeleteCacheWhenDone_checkBox.isChecked():
            self.config["DeleteCacheWhenDone"] = True
        else:
            self.config["DeleteCacheWhenDone"] = False
        self.config.update({"DeleteCacheWhenDone": self.config["DeleteCacheWhenDone"]})

    def onProjectsOrCollections(self):
        """Toggle between browsing projects and collections."""
        tree_rows = self.tree_management.source_model.rowCount()
        if self.ui.useCollections.isChecked():
            self.ui.ProjectLabel.setText("Collection")
            self.ui.ProjectComboBox.setEnabled(False)
            self.ui.ProjectComboBox.setVisible(False)
            self.ui.GroupLabel.setVisible(False)
            self.ui.GroupComboBox.setEnabled(False)
            self.ui.GroupComboBox.setVisible(False)
            self.ui.CollectionComboBox.setEnabled(True)
            self.ui.CollectionComboBox.setVisible(True)
            collection = self.ui.CollectionComboBox.currentData()
            if collection:
                self.collection = collection
                # Remove the rows from the tree and repopulate
                if tree_rows > 0:
                    self.tree_management.source_model.removeRows(0, tree_rows)
                self.collection_changed()
                self.tree_dialog.ui.treeView.setEnabled(True)
                self.ui.AnalysisPushButton.setEnabled(True)
            else:
                self.tree_dialog.ui.treeView.setEnabled(False)
                self.ui.AnalysisPushButton.setEnabled(False)

        else:
            self.ui.ProjectLabel.setText("Project")
            self.ui.ProjectComboBox.setEnabled(True)
            self.ui.ProjectComboBox.setVisible(True)
            self.ui.GroupLabel.setVisible(True)
            self.ui.GroupComboBox.setEnabled(True)
            self.ui.GroupComboBox.setVisible(True)
            self.ui.CollectionComboBox.setEnabled(False)
            self.ui.CollectionComboBox.setVisible(False)
            project = self.ui.ProjectComboBox.currentData()
            if project:
                self.project = project
                # Remove the rows from the tree and repopulate
                if tree_rows > 0:
                    self.tree_management.source_model.removeRows(0, tree_rows)
                self.project_changed()
                self.tree_dialog.ui.treeView.setEnabled(True)
                self.ui.AnalysisPushButton.setEnabled(True)
            else:
                self.tree_dialog.ui.treeView.setEnabled(False)
                self.ui.AnalysisPushButton.setEnabled(False)

    def show_setup(self):
        """_summary_."""
        self.setup.exec()

    def show_choose_app(self):
        """_summary_."""
        current_app = self.ui.AppName.text()
        if self.choose_app.exec():
            new_app = self.ui.AppName.text()
            if new_app != current_app:
                self.app_management.choose_the_app()

    def show_treeview(self, input_key, set_analysis=False):
        """_summary_.

        Args:
            input_key (_type_): _description_
            set_analysis (bool, optional): _description_. Defaults to False.
        """
        # TODO: if not analysis, make sure only a file can be returned
        self.tree_management.set_analysis = set_analysis
        if set_analysis:
            self.tree_dialog.setWindowTitle("Choose a Container (proj subj sess ...")
        else:
            self.tree_dialog.setWindowTitle("Choose a file")
        if self.tree_dialog.exec():
            if self.tree_management.current_item:
                if set_analysis:  # choosing analysis container
                    msg = (
                        f"{self.tree_management.current_item.container.container_type} "
                        f"{self.tree_management.current_item.container.label}"
                    )
                    self.ui.AnalysisPushButton.setText(msg)
                    self.ui.RunGearPushButton.setEnabled(True)
                    self.analysis_management.set_destination()
                    self.ui.tabWidget.setTabEnabled(0, True)
                    self.ui.tabWidget.setTabEnabled(1, True)
                    self.ui.AnalysisLabelLineEdit.setEnabled(True)
                    self.ui.plainTextEdit.appendPlainText(
                        "Select inputs and enter configuration, then press 'Run Gear'."
                    )
                    self.ui.tabWidget.setCurrentIndex(0)
                else:  # choosing input file
                    self.inputs[input_key]["pushButton"].setText(
                        self.tree_management.current_item.file.label
                    )
                    self.inputs[input_key]["item"] = self.tree_management.current_item
                    self.analysis_management.set_input(
                        input_key, self.tree_management.current_item.file
                    )

    def group_changed(self):
        """_summary_."""
        group = self.ui.GroupComboBox.currentData()
        if group:
            self.group = group
            log.info(f"group is {self.group.label}")
            # NOTE: It is possible to store a reference to a project as the
            #       ComboBox Data.
            #       e.g. ProjectComboBox.addItem(project.label, project)
            self.projects = self.group.projects()
            self.projects.sort(key=lambda x: x.label.lower())
            self.tree_management.source_model.clear()
            self.ui.ProjectComboBox.clear()
            self.project_labels = [p.label for p in self.projects]
            for project in self.projects:
                self.ui.ProjectComboBox.addItem(project.label, project)
            self.ui.ProjectComboBox.setCurrentIndex(-1)
            self.config.update({"group": self.group.label})
            self.ui.AnalysisPushButton.setEnabled(False)
            self.ui.plainTextEdit.appendPlainText("Select a project")

    def finish_project_setup(self):
        """_summary_."""
        self.ui.plainTextEdit.appendPlainText(
            "Choose analysis: select the project, a subject, session or acquisition "
            "where the results should be attached."
        )
        self.ui.AnalysisPushButton.setEnabled(True)

    def project_changed(self):
        """_summary_."""
        project = self.ui.ProjectComboBox.currentData()
        if project:
            self.tree_management.source_model.clear()
            self.project = project
            self.project_item = self.tree_management.populate_tree_from_project(project)
            log.info(f"project is {self.project.label}")
            self.config.update({"project": self.project.label})
        self.finish_project_setup()

    def finish_collection_setup(self):
        """_summary_."""
        self.ui.plainTextEdit.appendPlainText(
            "Choose analysis: select the session or "
            "acquisition where the results should be attached."
        )
        self.ui.AnalysisPushButton.setEnabled(True)

    def collection_changed(self):
        """_summary_."""
        collection = self.ui.CollectionComboBox.currentData()
        if collection:
            self.tree_management.source_model.clear()
            self.collection = collection
            self.collection_item = self.tree_management.populate_tree_from_collection(
                collection
            )
            log.info(f"collection is {self.collection.label}")
            self.config.update({"collection": self.collection.label})
        self.finish_collection_setup()

    def quit_fw_app_launcher(self, return_val):
        """_summary_.

        Args:
            return_val (_type_): _description_
        """
        if self.app_management.use_script_module:
            self.app_management.use_script_module.stop(self.app_management)
        if self.app_management.app_module:
            self.app_management.app_module.stop(self.app_management)
        sys.exit(return_val)
