# TODOs

## 2023-04-28 10:39:16

- [ ] Make a fully automated linux installer
  - [ ] See <https://makeself.io/>
- [ ] Cleanups
  - [ ] Completed Docstrings for every function and class
  - [ ] Divide README.md into multiple files (in `docs/`)
- [ ] Create local manifests in the `~/.config/` directory
  - [ ] e.g. `~/.config/fw-app-launcher/manifests/itksnap/manifest.json`
- [ ] Update VERSION_KEY in the config from the pyproject.toml file.
- [ ] Get files in common between multiple applications in the same installable
      repository/library.
  - fw_container_items.py
  - config.py
- [ ] Refactor code for consistency, efficiency, readability, and maintainability
  - [ ] Make README.md more concise with links to other directions.
