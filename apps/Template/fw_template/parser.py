"""Parser module to parse gear config.json."""
import json
from collections import OrderedDict
from pathlib import Path, PurePosixPath
from typing import Tuple


def parse_config(
    config_file: PurePosixPath,
) -> Tuple[str, str]:
    """Parse the gear_context's config.json file and return relevant inputs and options.

    The config.json file is created from the users choices in the "Run Gear" dialog.
    The user can select input files and set configuration parameters.

    Returns:
        list: A list of strings that are provided as arguments for the system command
              to run the local application
    """
    with open(config_file, "r") as fp:
        config = json.load(fp)

    params = list()

    if "config" in config:
        for key, val in config["config"].items():
            if len(key) == 1:
                params.extend(["-" + key, str(val)])
            else:
                params.extend(["--" + key, str(val)])

    # config.json says files are in /flywheel/v0/input/, but they are really where the config file is
    if "inputs" in config:
        for key, val in config["inputs"].items():
            new_path = Path(
                config_file.parent / "input" / key / val["location"]["name"]
            )
            params.extend(["-" + key, str(new_path)])

    return params
