"""__summary__."""

import logging
import os
from pathlib import Path

log = logging.getLogger(__name__)


def start(app_name):
    """__summary__."""
    log.debug("Starting %s Some_use use script", app_name)


def before(application):
    """__summary__."""
    app_name = application.ui.AppName.text()
    log.debug("Before %s %s", app_name, application.app_management.use_script)


def after(application):
    """__summary__."""
    app_name = application.ui.AppName.text()
    log.debug("After %s %s", app_name, application.app_management.use_script)


def stop(app_management):
    """__summary__."""
    app_name = app_management.main_window.ui.AppName.text()
    log.debug("Stopping %s %s", app_name, app_management.use_script)
