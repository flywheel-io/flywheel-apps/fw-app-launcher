#!/bin/bash

fw_app_launcher=$1

cp "$fw_app_launcher" "$HOME/.local/bin/fw_app_launcher"
chmod +x "$HOME/.local/bin/fw_app_launcher"

# Instructions from:
# https://medium.com/swlh/custom-protocol-handling-how-to-8ac41ff651eb
# Create a desktop shortcut
cp fw_app_launcher.desktop "$HOME"/.local/share/applications/fw_app_launcher.desktop

# Copy the mime type file
cat mimeapps.list >> "$HOME"/.local/share/applications/mimeapps.list

update-desktop-database "$HOME"/.local/share/applications