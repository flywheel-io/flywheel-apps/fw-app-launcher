"""Tool to convert default installer names to fully qualified installer names.

e.g.
fw_app_launcher-0.0.9-linux-amd64
fw_app_launcher-0.0.9-macosx-amd64.dmg
fw_app_launcher-0.0.9-windows-amd64.exe
"""
import argparse
import shutil
from pathlib import Path

import toml

APP_NAME = "fw_app_launcher"


def main(input_file, toml_file, os, arch, ext=None):
    """Rename installer file.

    Args:
        input_file (str): default path to the installer file.
        toml_file (str): Project TOML file where the version is defined.
        os (str): Operating system the installer is built for (linux, macosx, windows)
        arch (str): Architecture the installer is built on (e.g. "amd64")
        ext (str, optional): Extension of installer. Defaults to None.
    """
    input_file = Path(input_file)
    installer_root = input_file.parent
    with open(toml_file, "r") as f:
        data = toml.load(f)
        version_string = data["tool"]["poetry"]["version"]

    installer_name = f"{APP_NAME}-{version_string}-{os}-{arch}"
    if ext:
        installer_name += f".{ext}"
    installer_path = installer_root / installer_name
    shutil.move(input_file, installer_path)
    print(installer_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Create an installer name")
    parser.add_argument("-i", "--input_file", help="Input file", required=True)
    parser.add_argument("-t", "--toml", help="toml file", required=True)
    parser.add_argument("-o", "--os", help="operating system", required=True)
    parser.add_argument("-a", "--arch", help="architecture", required=True)
    parser.add_argument("-e", "--ext", help="extension", default=None)
    args = parser.parse_args()

    main(args.input_file, args.toml, args.os, args.arch, args.ext)
    print("Done")
