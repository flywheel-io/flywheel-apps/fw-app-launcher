#!/usr/bin/env bash
# This builds a single executable in ./dist/
if [ -f fw_app_launcher.spec ]; then
    echo "poetry run pyinstaller --noconfirm fw_app_launcher.spec "
    poetry run pyinstaller --noconfirm fw_app_launcher.spec 
else
    echo "poetry run pyinstaller (first time)"
    poetry run pyinstaller \
        -y \
        --onefile \
        --windowed \
        --icon resources/Flywheel.ico \
        --add-data apps:apps \
        --add-data resources:resources \
        --add-data pyproject.toml:. \
        --hidden-import=flywheel \
        --collect-all flywheel_gear_toolkit \
        --collect-all fw_meta \
        --collect-all fw_utils \
        --collect-all nibabel \
        fw_app_launcher.py
fi
poetry run python bin/create_installer_name.py \
    -i dist/fw_app_launcher \
    -t ./pyproject.toml \
    -o linux \
    -a amd64