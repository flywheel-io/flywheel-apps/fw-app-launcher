"""Unit tests for app_launcher.analysis_management.py."""
import datetime
import json
import logging
import os
import os.path as op
from pathlib import Path
from unittest.mock import MagicMock, patch

import flywheel
from dateutil.tz import tzutc
from PyQt6 import QtCore, QtGui, QtWidgets, uic

from management.analysis_management import AnalysisManagement
from management.app_management import AppManagement
from management.choose_app_window import ChooseApp
from management.config import Config
from management.fw_container_items import (
    ContainerParentModel,
    FileItem,
    FolderItem,
    ProjectItem,
)

ANALYSIS_ID = "621d12262813cf47e7c87027"
FILE_ENTRY = {
    "classification": {"Intent": ["Structural"], "Measurement": ["T1"]},
    "created": datetime.datetime(2022, 1, 10, 16, 14, 18, 609000, tzinfo=tzutc()),
    "deid_log_id": None,
    "file_id": "61dc5b5a68722e06deaf3e2b",
    "hash": "d33999cf1ede8fb45025f3de25c7501651731173036fcb49d65ed34b8dd3e975079e746c464a82fe261606e6c79dd853",
    "id": "114a472e-c431-43a8-9c2d-3de56f6b68e3",
    "info": {},
    "info_exists": True,
    "mimetype": "application/octet-stream",
    "modality": "MR",
    "modified": datetime.datetime(2022, 1, 10, 16, 14, 18, 609000, tzinfo=tzutc()),
    "name": "T1_AX_SE.nii.gz",
    "origin": {
        "id": "61dc5b4868722e06deaf3de7",
        "method": None,
        "name": None,
        "type": "job",
        "via": None,
    },
    "parent_ref": {"id": "61dc58ec68722e06deaf37d3", "type": "acquisition"},
    "parents": {
        "acquisition": "61dc58ec68722e06deaf37d3",
        "analysis": None,
        "group": "scien",
        "project": "61dc586368722e06deaf3532",
        "session": "61dc58ec68722e06deaf37d2",
        "subject": "61dc58ec68722e06deaf37d1",
    },
    "provider_id": "6047c661d4a7303a0bfe09d3",
    "replaced": None,
    "size": 1904676,
    "tags": [],
    "type": "nifti",
    "version": 1,
    "zip_member_count": None,
}

CORRECT_ANSWER = {
    "g": {
        "hierarchy": {"id": "61dc58ec68722e06deaf37d3", "type": "project"},
        "object": {
            "type": "nifti",
            "mimetype": "application/octet-stream",
            "modality": "MR",
            "classification": {"Intent": ["Structural"], "Measurement": ["T1"]},
            "tags": [],
            "info": {},
            "size": 1904676,
            "zip_member_count": None,
            "version": 1,
            "file_id": "61dc5b5a68722e06deaf3e2b",
            "origin": {"type": "job", "id": "61dc5b4868722e06deaf3de7"},
        },
        "location": {
            "path": f"/tmp/flywheel_cache_test/ga.ce.flywheel.io/Analyses/{ANALYSIS_ID}/input/g",
            "name": "T1_AX_SE.nii.gz",
        },
        "base": "file",
    }
}

source_dir = Path(op.dirname(os.path.realpath(__file__))).parent
cache_dir = Path("/tmp/flywheel_cache_test/ga.ce.flywheel.io")


class AppLauncher(QtWidgets.QMainWindow):
    def __init__(self):
        """Initialize application."""
        super(AppLauncher, self).__init__()
        self.source_dir = source_dir
        Form, _ = uic.loadUiType(source_dir / "resources/app_launcher.ui")
        self.ui = Form()
        self.ui.setupUi(self)
        self.config = Config()
        self.inputs = None
        self.configs = None
        self.choose_app = ChooseApp(self)
        self.app_management = AppManagement(self, self.choose_app)
        self.analysis_management = AnalysisManagement(self)


def test_set_input_works():
    """ """

    app = QtWidgets.QApplication([])
    app.setWindowIcon(QtGui.QIcon(str(source_dir / "resources/flywheel.png")))
    app_launcher = AppLauncher()

    source_model = ContainerParentModel()
    source_model.set_cache_dir(cache_dir)
    project = flywheel.models.project.Project()
    project.parents = dict()
    project.parents["group"] = "groupLabel"
    project.parents["project"] = "ProjectLabel"
    project.parents["subject"] = None
    project.parents["session"] = None
    project.parents["acquisition"] = None
    project.id = FILE_ENTRY["parent_ref"]["id"]
    project_item = ProjectItem(source_model, project)
    parent_item = FolderItem(project_item, "a Folder")
    file_entry = flywheel.models.file_entry.FileEntry(
        id=FILE_ENTRY["id"],
        name=FILE_ENTRY["name"],
        file_id=FILE_ENTRY["file_id"],
        version=FILE_ENTRY["version"],
        type=FILE_ENTRY["type"],
        mimetype=FILE_ENTRY["mimetype"],
        modality=FILE_ENTRY["modality"],
        classification=FILE_ENTRY["classification"],
        tags=FILE_ENTRY["tags"],
        info=FILE_ENTRY["info"],
        size=FILE_ENTRY["size"],
        zip_member_count=FILE_ENTRY["zip_member_count"],
    )
    file_entry._parent = project
    file_origin = flywheel.models.file_origin.FileOrigin(
        id=FILE_ENTRY["origin"]["id"], type=FILE_ENTRY["origin"]["type"]
    )
    file_entry.origin = file_origin
    current_item = FileItem(parent_item, file_entry)
    input_key = "g"
    app_launcher.analysis_management.analysis_dir = (
        cache_dir / "Analyses/621d12262813cf47e7c87027"
    )
    app_launcher.analysis_management.config_file = (
        app_launcher.analysis_management.analysis_dir / "config.json"
    )
    for dir_name in ["input", "output", "work"]:
        (app_launcher.analysis_management.analysis_dir / dir_name).mkdir(
            parents=True, exist_ok=True
        )
    app_launcher.analysis_management.gear_config = {
        "inputs": {},
        "destination": FILE_ENTRY["parent_ref"],
        "object": {"origin": {}},
        "config": {},
    }
    app_launcher.analysis_management.set_input(input_key, current_item)

    with open(app_launcher.analysis_management.config_file, "r") as ff:
        on_disk_dict = json.load(ff)
    assert on_disk_dict["inputs"] == CORRECT_ANSWER
