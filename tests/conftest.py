import json
import os
import shutil
import tarfile
from pathlib import Path

import pytest

source_dir = Path(os.path.dirname(os.path.realpath(__file__))).parent
cache_dir = Path(source_dir / "tests/cache")


class AnalysisManagement:
    def __init__(self):
        self.analysis_dir = None
        self.config_file = None
        self.file_mod_times = None


class AppManagement:
    def __init__(self):
        self.manifest = None
        self.use_script = "Segmentation"


class AppName:
    def __init__(self, app_name):
        self.name = app_name

    def text(self):
        return self.name


class UI:
    def __init__(self, app_name):
        self.AppName = AppName(app_name)


class AppLauncher:
    def __init__(self, app_name):
        self.analysis_management = AnalysisManagement()
        self.app_management = AppManagement()
        self.ui = UI(app_name)


@pytest.fixture
def skip_message():
    def _method(instance):
        message = ""
        user_json = Path(Path.home() / ".config/flywheel/user.json")
        if not user_json.exists():
            message = f"No API key available in {str(user_json)}"
        with open(user_json) as json_file:
            data = json.load(json_file)
            if "key" not in data:
                message = "Not logged in to a Flywheel instance."
            elif instance not in data["key"]:
                message = f"Not logged in to {instance}."
        return message

    return _method


def _install_assets(tar_name):
    """Remove any old test analysis and create clean example for all following tests."""
    asset_dir = Path(source_dir / "tests/assets")
    cache_dir = Path(source_dir / "tests/cache")
    if cache_dir.exists():
        shutil.rmtree(cache_dir)
    with tarfile.open(asset_dir / tar_name, "r") as tar:
        tar.extractall(path=cache_dir)


@pytest.fixture
def install_assets():
    def _method(tar_name):
        _install_assets(tar_name)

    return _method


@pytest.fixture
def create_application():
    def _method(app_name, tar_name):
        application = AppLauncher(app_name)
        _install_assets(tar_name)
        application.analysis_management.analysis_dir = (
            cache_dir / "626176972813cf2cfe9940fd"
        )
        application.analysis_management.config_file = (
            cache_dir / "626176972813cf2cfe9940fd/config.json"
        )

        manifest_file = source_dir / f"apps/{app_name}/manifest.json"
        with open(manifest_file, "r") as stream:
            application.app_management.manifest = json.load(stream)

        return application

    return _method


@pytest.fixture
def print_captured():
    def _method(captured):
        """Show what has been captured in std out and err."""

        print("\nout")
        for ii, msg in enumerate(captured.out.split("\n")):
            print(f"{ii:2d} {msg}")
        print("\nerr")
        for ii, msg in enumerate(captured.err.split("\n")):
            print(f"{ii:2d} {msg}")

    return _method
