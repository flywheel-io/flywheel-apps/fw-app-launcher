"""Unit tests for running itk-snap."""
import json
import os
from pathlib import Path

import pytest
from apps.ITK_SNAP.fw_gear_itk_snap.parser import parse_config
from apps.ITK_SNAP.run import main
from apps.ITK_SNAP.uses.URL.run import after, before

source_dir = Path(os.path.dirname(os.path.realpath(__file__))).parent

from fw_app_launcher import AppLauncher, fw_application

# def test_run_via_url_itk_snap_works(create_application):
#     """ """
#     app = fw_application(
#         ["/Applications/fw_app_launcher.app/Contents/MacOS/fw_app_launcher"]
#     )
#     application = AppLauncher(app)
#     application.run_via_url(
#         "fwlaunch://itk_snap?use=URL&destination=639266a18cdd05613d904114&file_name=T1_SAG_SE.nii.gz"
#     )
#     assert 0
