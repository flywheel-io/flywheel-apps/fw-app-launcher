"""Unit tests for running itk-snap."""
import json
import os
from pathlib import Path

import pytest

from apps.ITK_SNAP.fw_gear_itk_snap.parser import parse_config
from apps.ITK_SNAP.run import main
from apps.ITK_SNAP.uses.Segmentation.run import after, before

source_dir = Path(os.path.dirname(os.path.realpath(__file__))).parent

CORRECT_PARAM = [
    "-z",
    "1",
    "--threads",
    "3",
    "--scale",
    "1",
    "--geometry",
    "100x200+10+20",
    "-g",
    str(source_dir / "tests/cache/626176972813cf2cfe9940fd/input/g/T1_AX_SE.nii.gz"),
    "-s",
    str(source_dir / "tests/cache/626176972813cf2cfe9940fd/input/s/seg.nii.gz"),
    "-l",
    str(
        source_dir
        / "tests/cache/626176972813cf2cfe9940fd/input/l/ITKsnap_labels_2021.txt"
    ),
    "-o",
    str(source_dir / "tests/cache/626176972813cf2cfe9940fd/input/o1/T1_COR_SE.nii.gz"),
    str(source_dir / "tests/cache/626176972813cf2cfe9940fd/input/o2/T1_SAG_SE.nii.gz"),
]

MOD_TIMES = {
    "config.json": 1655303097.0,
    "input/o2/T1_SAG_SE.nii.gz": 1650639143.0,
    "input/g/T1_AX_SE.nii.gz": 1650554561.0,
    "input/s/seg.nii": 1650569715.0,
    "input/o1/T1_COR_SE.nii.gz": 1650639130.0,
    "input/l/ITKsnap_labels_2021.txt": 1650554562.0,
    "output/seg.nii.gz": 1650579475.0,
}


def test_itk_snap_seg_before_works(create_application):
    """ """
    application = create_application("ITK_SNAP", "run_itk_snap.tgz")

    before(application)

    assert application.analysis_management.file_mod_times == MOD_TIMES
    assert len(application.analysis_management.file_mod_times) == 7


def test_parse_config_works(create_application, capfd, print_captured):
    """ """

    application = create_application("ITK_SNAP", "run_itk_snap.tgz")

    params = parse_config(application.analysis_management.config_file)

    print(params)
    print("\n\n")
    print(CORRECT_PARAM)
    captured = capfd.readouterr()
    print_captured(captured)

    assert params == CORRECT_PARAM


def test_run_itk_snap_works(skip_message, create_application):
    """ """

    msg = skip_message("ga")
    if msg != "":
        pytest.skip(msg)

    application = create_application("ITK_SNAP", "run_itk_snap.tgz")

    stdout, stderr, error_code = main(application)

    assert error_code == 0


def test_itk_snap_seg_after_works(create_application):
    """ """
    application = create_application("ITK_SNAP", "run_itk_snap.tgz")
    application.analysis_management.file_mod_times = MOD_TIMES
    application.analysis_management.file_mod_times["input/s/seg.nii"] = (
        1650569715.0 - 715.0
    )

    after(application)

    full_path = source_dir / "tests/cache/626176972813cf2cfe9940fd/output/seg.nii"
    mod_time = os.path.getmtime(full_path)
    assert mod_time == 1650569715.0
